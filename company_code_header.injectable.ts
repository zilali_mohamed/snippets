import { Injectable } from '@angular/core';
import { NavigationStart, Router, Event } from "@angular/router";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class HeaderService {

  public _businessProg: BehaviorSubject<string>;

  constructor(private _router: Router) {
    this._businessProg = new BehaviorSubject<string>('');
  }

  setBusinessProg(param) {
    this._businessProg.next(param);
  }
}