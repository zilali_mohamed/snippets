(function ($) {
    'use strict';

    if (!$.version || $.version.major < 2) {
        throw new Error('This version requires OpenSeadragon version 2.0.0+');
    }
    $.Viewer.prototype.tileOverlay = function (options) {
        if (!this.tileOverlayInstance || options) {
            options = options || {};
            options.viewer = this;
            this.tileOverlayInstance = new $.TileOverlay(options);
        }
        return this.tileOverlayInstance;
    };


    $.TileOverlay = function (options) {

        $.extend(true, this, {
            cornsImgs: null,
            onClicked: null

        }, options);

        this.element = $.makeNeutralElement('div');
        this.selectelement = $.makeNeutralElement('div');
        // this.element.style.background = 'rgba(0, 0, 0, 0.5)';
        this.element.className = 'hexa-box';



        if (navigator.userAgent.match(/mobile/i)) {
            new $.MouseTracker({
                element: this.viewer.canvas,
                clickHandler: move.bind(this)
            });
        } else {
            new $.MouseTracker({
                element: this.viewer.canvas,
                moveHandler: move.bind(this)
            });
        }


        this.borders = this.borders || [];  
        this.corners = [];

        for (var i = 0; i < this.cornsImgs.length; i++) {
            if(this.cornsImgs[i]){ 
                this.corners[i] = $.makeNeutralElement('IMG');
                this.corners[i].className = 'img-' + i + '-handle';
                this.corners[i].style.position = 'absolute';
                if(i === 0 || i === 3){
                    this.corners[i].style.width = '11.0%';
                    this.corners[i].style.height = '29.80%';
                }else{
                    this.corners[i].style.width = '23.2%';
                    this.corners[i].style.height = '15.34%';
                }
                // this.corners[i].style.background = '#000';
                this.corners[i].src = this.cornsImgs[i].icon.REST;

                new $.MouseTracker({
                    element: this.corners[i],
                    clickHandler: this.onClick.bind(this, i),
                    enterHandler: this.onEnter.bind(this, i),
                    exitHandler: this.onExit.bind(this, i)
                });
                this.element.appendChild(this.corners[i]);
            }
        }

        

        this.select_elt = $.makeNeutralElement('IMG');
        this.select_elt.className = 'img-' + i + '-handle';
        this.select_elt.style.position = 'absolute'; 
        this.select_elt.style.width = '23.2%';
        this.select_elt.style.height = '15.34%'; 
        this.select_elt.style.top = '0px';
        this.select_elt.style.left = '15.8%';
        this.select_elt.src = this.cornsImgs[1].icon.REST;
        new $.MouseTracker({
                element: this.select_elt,
                clickHandler: this.onClick.bind(this, 1),
                enterHandler: this.onEnter.bind(this, 1),
                exitHandler: this.onExit.bind(this, 1)
        });
        this.selectelement.appendChild(this.select_elt);
        
        if(this.corners[0]){
            this.corners[0].style.top = '35.7%';
            this.corners[0].style.left = '0px'; 
        }

        this.corners[1].style.top = '0px';
        this.corners[1].style.left = '15.8%';

        this.corners[2].style.top = '0px';
        this.corners[2].style.right = '15.8%';

        if(this.corners[3]){
            this.corners[3].style.bottom = '35.4%';
            this.corners[3].style.right = '-1px'; 
        }
        if(this.corners[4]){
            this.corners[4].style.bottom = '-1px';
            this.corners[4].style.right = '15.6%';
        }
        if(this.corners[5]){
            this.corners[5].style.bottom = '-2px';
            this.corners[5].style.left = '15.8%';
        }
        if (!this.overlay) {
            this.overlay = new $.Overlay(this.element, this.rect || new $.Rect());
            this.selectOverlay = new $.Overlay(this.selectelement, this.rect || new $.Rect())
        }


        this.viewer.addHandler('overlay-hexa-click', this.onClicked);


        this.viewer.addHandler('open', this.draw.bind(this));
        this.viewer.addHandler('animation', this.draw.bind(this));
        this.viewer.addHandler('resize', this.draw.bind(this));
        this.viewer.addHandler('rotate', this.draw.bind(this));
    };

    $.extend($.TileOverlay.prototype, $.ControlDock.prototype, {

        draw: function () {
            if (this.rect) {
                if(this.is_selecting){
                    this.selectOverlay.update(this.rect);
                    this.selectOverlay.drawHTML(this.viewer.drawer.container, this.viewer.viewport);
                }else{
                    this.overlay.update(this.rect);
                    this.overlay.drawHTML(this.viewer.drawer.container, this.viewer.viewport);
                }
            }
            return this;
        },
        undraw: function () {
            if(this.overlay)
                this.overlay.destroy();
            if(this.selectOverlay) 
                this.selectOverlay.destroy();
            this.rect = null;
            return this;
        },
        onClick: function (i) {
            resetOverlay(this);
            this.viewer.raiseEvent('overlay-hexa-click', { crop: this.currentCrop, action: this.cornsImgs[i].label });
        },
        onEnter: function (i) {
            this.corners[i].src = this.cornsImgs[i].icon.DOWN;
            if(i === 1) this.select_elt.src = this.cornsImgs[i].icon.DOWN;
        },
        onExit: function (i) {
            this.corners[i].src = this.cornsImgs[i].icon.REST;
            if(i === 1) this.select_elt.src = this.cornsImgs[i].icon.REST;
        },
        setSelecting :function (value) {
            this.is_selecting = value;
        },
        getSelecting:function () {
            return this.is_selecting;
        } 
    });

    function move(event) {

        var pos = this.viewer.viewport.pointFromPixel(event.position);

        var photo = _.find(this.viewer.world._items, function (v) {
            var box = v.getBounds();
            return (v.compositeOperation !== null && pos.x > box.x && pos.y > box.y && pos.x < box.x + box.width && pos.y < box.y + box.height);
        });
        if (photo && photo.getOpacity() !== 0.6) {

            var box = photo.getBounds();
            this.rect = box;
            this.viewer.world._items = _.without(this.viewer.world._items, photo); 
            this.viewer.world._items.splice(1, 0, photo);

            if (this.prevhovredtile) {
                if (photo.compositeOperation._id !== this.prevhovredtile.compositeOperation._id) {
                    this.draw(); 
                    this.prevhovredtile = photo;
                    this.currentCrop = photo.compositeOperation;

                }
            } else {
                this.draw(); 
                this.prevhovredtile = photo;
                this.currentCrop = photo.compositeOperation; 
            }

        } else { 

            resetOverlay(this); 
            this.prevhovredtile = undefined;

        }
    }

    function resetOverlay(self) {
        for (var i = 0; i < self.cornsImgs.length; i++) {
            if (self.corners[i] && self.cornsImgs[i]) {
                self.corners[i].src = self.cornsImgs[i].icon.REST;
            }
        }
        self.select_elt.src = self.cornsImgs[1].icon.REST;
        
        self.undraw(); 
    }  
})(OpenSeadragon);