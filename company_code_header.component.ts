import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { HeaderService } from "./header.injectable";
@Component({
  selector: 'appHeader',
  templateUrl: './header.component.html',
  styles: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(
    private router: Router,
    private headerService: HeaderService,
  ) {
    this.headerService._businessProg.subscribe(params => {
      if (params.split('.')[0] === 'kill') {
        /* some processing */
      }
    });
  }
}