import { Component, OnInit, Directive, HostBinding, Input, NgZone } from '@angular/core';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'business',
  templateUrl: 'business.component.html'
})

export class BusinessComponent implements OnInit {

  mosReady: Subscription;

  constructor(
    private headerService: HeaderService,
  ) { }

  ngOnInit() {
    this.mosReady = this.headerService._businessProg.subscribe((params) => {
      if (params.split('.')[0] === 'make' && params.split('.')[1]) {
        console.log('make headerService._businessProg');


      } else if (params.split('.')[0] === 'kill' && params.split('.')[1]) {
        console.log('kill headerService._businessProg');


      } else {
        console.log("no progresss");
      }
    });
  }
  ngOnDestroy() {
    if (this.mosReady) this.mosReady.unsubscribe();
  }
}

