
var assert = require('assert'),
  sharp = require('sharp'),
  MongoClient = require('mongodb').MongoClient;


var data_base_url = 'mongodb://localhost:port/dbname',
server = "path/to/server";

MongoClient.connect(data_base_url, function (err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
  var collection = db.collection('collection'); 
  var i = 0, rgbdocs = [], nbr = 0;
  var records = collection.find({}, { "pathImage": 1 });
  records.count(function (err, count) {
    console.log(count)
    assert.equal(err, null);
    records.forEach(function (doc) { 
      sharp(server + doc.pathImage).resize(500, 453)
        .crop(sharp.strategy.entropy)
        .background('#000000')
        .flatten()
        .png()
        .overlayWith('src/base2.png', { cutout: true })
        .toFile('output/' + doc._id + '.png', function (err, info) {
          if (err) console.log(err)
          if (info) console.log(info)
        })
      i += 1;
      print((100 * i / count));
      if (i >= count) {
        db.close();
      } 
    });
  });
});

function print(progress) {
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write(progress + '%');
}
