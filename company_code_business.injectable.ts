import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HeaderService } from "../path/to/header.injectable";

@Injectable()
export class BusinessService {
    private isProcessing = '';
    constructor(
        private headerService: HeaderService
    ) { }

    makeBusiness(args) {
        this.isProcessing = args;
        this.headerService.setBusinessProg("make." + args);

        Meteor.call('function.name.onserver', { params: [] /* params... */ }, (err, res) => {
            if (err) {
                console.log(err);
            } else {
                if (res && res.error) {
                    alert(res.error)
                } else {
                    this.isProcessing = "";
                    this.headerService.setBusinessProg("kill." + args);
                }
            }
        });
    }

    getPregress() {
        return this.isProcessing;
    }
}

